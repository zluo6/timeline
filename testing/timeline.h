#ifndef TIMELINE_H
#define TIMELINE_H

#include <QtWidgets/QWidget>
#include <QtGui/QtGui>

class TimeLine : public QWidget
{
	Q_OBJECT

public:
	TimeLine(QWidget *parent);
	~TimeLine();
	void getInput(int BNS, int exp, int cap);

protected:
	void paintEvent(QPaintEvent * e);

private:
	int w;
	int h;
	int BNSDelay;
	int exposure;
	int capDelay;

	void drawBox(QPainter &painter, QPen &pen, QRect &rect);
	void drawLine(QPainter &painter, QPen &pen, QRect &rect);
	void drawMark(QPainter &painter, QPen &pen, QRect &rect);
};

#endif // TIMELINE_H
