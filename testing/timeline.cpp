#include "timeline.h"

TimeLine::TimeLine(QWidget *parent) : QWidget(parent)
{
	w = this->width();
	h = this->height();
	BNSDelay = 10;
	exposure = 20;
	capDelay = 50;
	update();
}

TimeLine::~TimeLine()
{

}

void TimeLine::getInput(int BNS, int exp, int cap)
{
	BNSDelay = BNS;
	exposure = exp;
	capDelay = cap;
	update();
}

void TimeLine::paintEvent(QPaintEvent * e)
{
	QPainter painter(this);
	QPen pen(Qt::black, 1, Qt::SolidLine);
	QRect rect = contentsRect();
	drawBox(painter, pen, rect);
	drawLine(painter, pen, rect);
	drawMark(painter, pen, rect);
}

void TimeLine::drawBox(QPainter &painter, QPen &pen, QRect &rect)
{
	pen.setWidth(4);
	pen.setColor(Qt::darkGray);
	painter.setPen(pen);
	painter.drawLine(rect.left()+5, rect.top()+5, rect.right()-5, rect.top()+5);
	painter.drawLine(rect.left()+5, rect.top()+5, rect.left()+5, rect.bottom()-5);
	painter.drawLine(rect.right()-5, rect.top()+5, rect.right()-5, rect.bottom()-5);
	painter.drawLine(rect.left()+5, rect.bottom()-5, rect.right()-5, rect.bottom()-5);
}

void TimeLine::drawLine(QPainter &painter, QPen &pen, QRect &rect)
{
	pen.setWidth(2);
	pen.setColor(Qt::darkBlue);
	painter.setPen(pen);
	int hmid = (rect.top()+rect.bottom())/2;
	int wid = rect.right()-rect.left();
	painter.drawLine(rect.left()+5+wid/10, hmid, rect.right()-5-wid/10, hmid);
	painter.drawLine(rect.right()-5-wid/10, hmid, rect.right()-15-wid/10, hmid-5);
	painter.drawLine(rect.right()-5-wid/10, hmid, rect.right()-15-wid/10, hmid+5);
	pen.setColor(Qt::black);
	painter.setPen(pen);
	painter.drawText(rect.right()-15-wid/10, hmid+10, 30, 15, Qt::AlignCenter, "t/ms");
}
void TimeLine::drawMark(QPainter &painter, QPen &pen, QRect &rect)
{
	pen.setWidth(1);
	pen.setColor(Qt::darkBlue);
	painter.setPen(pen);

	int hmid = (rect.top()+rect.bottom())/2;
	int wid = rect.right()-rect.left();
	int xstart = rect.left()+5+wid/10;
	int xend = rect.right()-5-wid/10;

	int max = (BNSDelay+exposure>capDelay)?(BNSDelay+exposure):capDelay;

	int xBNS = xstart;
	int wBNS = BNSDelay*(xend-xstart-20)/(max);
	int xEXP = xBNS+wBNS;
	int wEXP = exposure*(xend-xstart-20)/(max);
	int wCAP = capDelay*(xend-xstart-20)/(max);

	QBrush brush(Qt::yellow);
	painter.fillRect(xBNS, hmid-30, wBNS, 29, brush);
	painter.drawLine(xBNS, hmid, xBNS, hmid-30);
	painter.drawLine(xBNS, hmid-30, xBNS+wBNS, hmid-30);
	painter.drawLine(xBNS+wBNS, hmid, xBNS+wBNS, hmid-30);
	painter.drawText(xBNS, hmid-30, wBNS, 30, Qt::AlignCenter, QString::number(BNSDelay));
	painter.drawLine(xBNS, hmid, xBNS+10, hmid+10);
	painter.drawLine(xBNS+wBNS, hmid, xBNS+wBNS-10, hmid+10);
	painter.drawText(xBNS, hmid, wBNS, 30, Qt::AlignCenter, "BNS Delay");

	brush.setColor(Qt::cyan);
	painter.fillRect(xEXP, hmid-30, wEXP, 29, brush);
	painter.drawLine(xEXP, hmid, xEXP, hmid-30);
	painter.drawLine(xEXP, hmid-30, xEXP+wEXP, hmid-30);
	painter.drawLine(xEXP+wEXP, hmid, xEXP+wEXP, hmid-30);
	painter.drawText(xEXP, hmid-30, wEXP, 30, Qt::AlignCenter, QString::number(exposure));
	painter.drawLine(xEXP, hmid, xEXP+10, hmid+10);
	painter.drawLine(xEXP+wEXP, hmid, xEXP+wEXP-10, hmid+10);
	painter.drawText(xEXP, hmid, wEXP, 30, Qt::AlignCenter, "Exposure");

	pen.setWidth(2);
	pen.setColor(Qt::red);
	painter.setPen(pen);	
	painter.drawLine(xBNS, hmid-30, xBNS, hmid-1);
	painter.drawLine(xBNS+wCAP, hmid-30, xBNS+wCAP, hmid-1);
	painter.drawLine(xBNS, hmid-30, xBNS+10, hmid-40);
	painter.drawLine(xBNS+wCAP, hmid-30, xBNS+wCAP-10, hmid-40);
	painter.drawText(xBNS, hmid-60, wCAP, 30, Qt::AlignCenter, "CAP Delay: "+QString::number(capDelay));

}
