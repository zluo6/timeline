#ifndef TESTING_H
#define TESTING_H

#include <QtWidgets/QMainWindow>
#include "ui_testing.h"
#include "timeline.h"
#include <QtWidgets/QGridLayout>

class testing : public QMainWindow
{
	Q_OBJECT

public:
	testing(QWidget *parent = 0);
	~testing();

private:
	Ui::testingClass ui;
	TimeLine * myTime;
};

#endif // TESTING_H
