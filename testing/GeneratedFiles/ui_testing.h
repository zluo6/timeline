/********************************************************************************
** Form generated from reading UI file 'testing.ui'
**
** Created by: Qt User Interface Compiler version 5.0.2
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_TESTING_H
#define UI_TESTING_H

#include <QtCore/QVariant>
#include <QtWidgets/QAction>
#include <QtWidgets/QApplication>
#include <QtWidgets/QButtonGroup>
#include <QtWidgets/QHeaderView>
#include <QtWidgets/QMainWindow>
#include <QtWidgets/QMenuBar>
#include <QtWidgets/QWidget>

QT_BEGIN_NAMESPACE

class Ui_testingClass
{
public:
    QWidget *centralWidget;
    QMenuBar *menuBar;

    void setupUi(QMainWindow *testingClass)
    {
        if (testingClass->objectName().isEmpty())
            testingClass->setObjectName(QStringLiteral("testingClass"));
        testingClass->resize(600, 400);
        centralWidget = new QWidget(testingClass);
        centralWidget->setObjectName(QStringLiteral("centralWidget"));
        testingClass->setCentralWidget(centralWidget);
        menuBar = new QMenuBar(testingClass);
        menuBar->setObjectName(QStringLiteral("menuBar"));
        menuBar->setGeometry(QRect(0, 0, 600, 21));
        testingClass->setMenuBar(menuBar);

        retranslateUi(testingClass);

        QMetaObject::connectSlotsByName(testingClass);
    } // setupUi

    void retranslateUi(QMainWindow *testingClass)
    {
        testingClass->setWindowTitle(QApplication::translate("testingClass", "testing", 0));
    } // retranslateUi

};

namespace Ui {
    class testingClass: public Ui_testingClass {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_TESTING_H
