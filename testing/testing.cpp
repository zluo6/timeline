#include "testing.h"

testing::testing(QWidget *parent)
	: QMainWindow(parent)
{
	ui.setupUi(this);
	myTime = new TimeLine(this);

	QGridLayout *layout = new QGridLayout;
	QSizePolicy sizePolicy(QSizePolicy::Fixed, QSizePolicy::Fixed);

	myTime->setSizePolicy(sizePolicy);
	myTime->setMinimumSize(QSize(400, 150));
	layout->addWidget(myTime, 0, 0, 1, 1);

	setLayout(layout);

	myTime->update();
}

testing::~testing()
{

}
